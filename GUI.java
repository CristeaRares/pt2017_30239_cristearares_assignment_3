import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class GUI {

	private JFrame frmManagerComenzi;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTable table;
    private Warehouse w=new Warehouse();
    private OPDept o=new OPDept();
    private JTable table_1;
    private JTextField textField_3;
    private JTable table_2;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTable table_3;
    private JTextField textField_6;
    private JTextField textField_7;
    private JTextField textField_8;
    private JTextField textField_9;
    private JTextField textField_10;
    private JTextField textField_11;
   
   
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmManagerComenzi.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmManagerComenzi = new JFrame();
		frmManagerComenzi.setTitle("Manager Comenzi");
		frmManagerComenzi.setBounds(100, 100, 470, 320);
		frmManagerComenzi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmManagerComenzi.getContentPane().setLayout(new CardLayout(0, 0));
		
		JPanel Main = new JPanel();
		frmManagerComenzi.getContentPane().add(Main, "name_1068300757127797");
		Main.setLayout(null);
		
		JPanel Administrator = new JPanel();
		frmManagerComenzi.getContentPane().add(Administrator, "name_994173063579583");
		Administrator.setLayout(null);
		
		JPanel Client = new JPanel();
		frmManagerComenzi.getContentPane().add(Client, "name_1068680972965922");
		Client.setLayout(null);
		
		
		
		 JPanel AfisareProduse = new JPanel();
		frmManagerComenzi.getContentPane().add(AfisareProduse, "name_994174660272970");
		AfisareProduse.setLayout(null);
		
		JPanel AdaugareProdus = new JPanel();
		frmManagerComenzi.getContentPane().add(AdaugareProdus, "name_996223324453406");
		AdaugareProdus.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 175);
		AfisareProduse.add(scrollPane);
		
		table = new JTable();
		DefaultTableModel tableModel=new DefaultTableModel(
				new Object[][] {
					
				},
				new String[] {
					"Nume-Produs", "Cantitate", "Pret"
				}
			);
		table.setModel(tableModel);
		
		scrollPane.setViewportView(table);
				
		JPanel UnderStock = new JPanel();
		frmManagerComenzi.getContentPane().add(UnderStock, "name_1050752746904327");
		UnderStock.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 414, 192);
		UnderStock.add(scrollPane_1);
		
		table_1 = new JTable();
		DefaultTableModel tableModel1=new DefaultTableModel(
				new Object[][] {
					
				},
				new String[] {
					"Nume-Produs", "Cantitate", "Pret"
				}
			);
		table_1.setModel(tableModel1);
		scrollPane_1.setViewportView(table_1);
		
		
		textField = new JTextField();
		textField.setBounds(202, 28, 112, 20);
		AdaugareProdus.add(textField);
		textField.setColumns(10);
		
		JLabel lblNumeProdus = new JLabel("Nume Produs");
		lblNumeProdus.setBounds(94, 31, 86, 14);
		AdaugareProdus.add(lblNumeProdus);
		
		textField_1 = new JTextField();
		textField_1.setBounds(202, 77, 112, 20);
		AdaugareProdus.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCantitate = new JLabel("Cantitate");
		lblCantitate.setBounds(94, 80, 70, 14);
		AdaugareProdus.add(lblCantitate);
		
		textField_2 = new JTextField();
		textField_2.setBounds(202, 133, 112, 20);
		AdaugareProdus.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(223, 46, 86, 20);
		Administrator.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblCantitateunderstock = new JLabel("CantitateUnderStock");
		lblCantitateunderstock.setBounds(319, 49, 127, 14);
		Administrator.add(lblCantitateunderstock);
		
		textField_4 = new JTextField();
		textField_4.setBounds(223, 111, 86, 20);
		Administrator.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblCantitateoverstock = new JLabel("CantitateOverStock");
		lblCantitateoverstock.setBounds(319, 114, 117, 14);
		Administrator.add(lblCantitateoverstock);
		
		JPanel OverStock = new JPanel();
		frmManagerComenzi.getContentPane().add(OverStock, "name_1052978101773262");
		OverStock.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 11, 414, 195);
		OverStock.add(scrollPane_2);
		
		table_2 = new JTable();
		DefaultTableModel tableModel2=new DefaultTableModel(
				new Object[][] {
					
				},
				new String[] {
					"Nume-Produs", "Cantitate", "Pret"
				}
			);
		table_2.setModel(tableModel2);
		scrollPane_2.setViewportView(table_2);
		
		JLabel lblPret = new JLabel("Pret");
		lblPret.setBounds(94, 136, 70, 14);
		AdaugareProdus.add(lblPret);
		
		JLabel lblNumeProdus1 = new JLabel("NumeProdus");
		lblNumeProdus1.setBounds(127, 114, 86, 14);
		Administrator.add(lblNumeProdus1);
		
		textField_5 = new JTextField();
		textField_5.setBounds(25, 111, 86, 20);
		Administrator.add(textField_5);
		textField_5.setColumns(10);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 129, 414, 88);
		Client.add(scrollPane_3);
		
		table_3 = new JTable();
		DefaultTableModel tableModel3=new DefaultTableModel(
				new Object[][] {
					
				},
				new String[] {
					"Id","Nume-Client","Nume-Produs", "Cantitate", "Plata"
				}
			);
		table_3.setModel(tableModel3);
		scrollPane_3.setViewportView(table_3);
		
		textField_6 = new JTextField();
		textField_6.setBounds(87, 52, 86, 20);
		Client.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblProdus = new JLabel("Produs");
		lblProdus.setBounds(33, 55, 46, 14);
		Client.add(lblProdus);
		
		textField_7 = new JTextField();
		textField_7.setBounds(318, 52, 86, 20);
		Client.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblCantitate_1 = new JLabel("Cantitate");
		lblCantitate_1.setBounds(235, 55, 73, 14);
		Client.add(lblCantitate_1);
		
		textField_8 = new JTextField();
		textField_8.setBounds(87, 21, 86, 20);
		Client.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblId = new JLabel("Id-Comanda");
		lblId.setBounds(33, 24, 46, 14);
		Client.add(lblId);
		
		textField_9 = new JTextField();
		textField_9.setBounds(318, 21, 86, 20);
		Client.add(textField_9);
		textField_9.setColumns(10);
		
		JLabel lblNumeclient = new JLabel("Nume-Client");
		lblNumeclient.setBounds(235, 24, 73, 14);
		Client.add(lblNumeclient);
		
		
		
		textField_10 = new JTextField();
		textField_10.setBounds(25, 176, 86, 20);
		Administrator.add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(25, 207, 86, 20);
		Administrator.add(textField_11);
		textField_11.setColumns(10);
		
		JLabel lblNumeProd = new JLabel("NumeProd");
		lblNumeProd.setBounds(127, 179, 64, 14);
		Administrator.add(lblNumeProd);
		
		JLabel lblPretNou = new JLabel("PretNou");
		lblPretNou.setBounds(127, 210, 64, 14);
		Administrator.add(lblPretNou);
		
		JButton btnAdaugaprodus = new JButton("AdaugaProdus");
		btnAdaugaprodus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 String n=textField.getText();
				   Integer i=Integer.parseInt(textField_1.getText());
				 for (final Product p:w.getProdus()){
					   Integer j=p.getCantitate()+i;
					    if(p.getNumeProdus().equals(n)){
					       p.setCantitate(j);	
					    }
					
					}
			   w.adaugaProdus(new Product(textField.getText(),Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_2.getText())));
			   w.writeFile();
			  
				
			}
		});
		btnAdaugaprodus.setBounds(160, 205, 120, 23);
		AdaugareProdus.add(btnAdaugaprodus);
		
		JButton btnInapoi = new JButton("Inapoi");
		btnInapoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.setVisible(false);
				Administrator.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
				
			}
		});
		btnInapoi.setBounds(10, 227, 89, 23);
		AdaugareProdus.add(btnInapoi);
		
		JButton btnInapoi_2 = new JButton("Inapoi");
		btnInapoi_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.setVisible(false);
				Administrator.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnInapoi_2.setBounds(10, 227, 89, 23);
		UnderStock.add(btnInapoi_2);
		
		
		JButton btnInapoi_1 = new JButton("Inapoi");
		btnInapoi_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.setVisible(false);
				Administrator.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnInapoi_1.setBounds(10, 227, 89, 23);
		AfisareProduse.add(btnInapoi_1);
		
		JButton btnInapoi_4 = new JButton("Inapoi");
		btnInapoi_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.setVisible(true);
				Administrator.setVisible(false);
				Client.setVisible(false);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnInapoi_4.setBounds(10, 227, 89, 23);
		Client.add(btnInapoi_4);
		
		
		
		JButton btnAfisareProduse = new JButton("Afisare Produse");
		btnAfisareProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.setVisible(false);
				tableModel.setRowCount(0);
				Administrator.setVisible(false);
				Client.setVisible(false);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(true);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
				for (final Product p:w.getProdus()){
					tableModel.addRow(new Object[]{p.getNumeProdus(),p.getCantitate(),p.getPret()});
					
				}
				

			}
		});
		btnAfisareProduse.setBounds(25, 11, 137, 23);
		Administrator.add(btnAfisareProduse);
		
		JButton btnAdaugareProdus = new JButton("Adaugare Produs");
		btnAdaugareProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.setVisible(false);
				Administrator.setVisible(false);
				AdaugareProdus.setVisible(true);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnAdaugareProdus.setBounds(25, 45, 137, 23);
		Administrator.add(btnAdaugareProdus);
		
		JButton btnUnderStock = new JButton("UnderStock");
		btnUnderStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableModel1.setRowCount(0);
				Main.setVisible(false);
				Administrator.setVisible(false);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(true);
				OverStock.setVisible(false);
				for (final Product p:w.getProdus()){
					int i=Integer.parseInt(textField_3.getText());
					if(p.getCantitate()<i){
					tableModel1.addRow(new Object[]{p.getNumeProdus(),p.getCantitate(),p.getPret()});
					}
				}
			}
		});
		btnUnderStock.setBounds(223, 11, 137, 23);
		Administrator.add(btnUnderStock);
		
		JButton btnOverStock = new JButton("OverStock");
		btnOverStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableModel2.setRowCount(0);
				Main.setVisible(false);
				Administrator.setVisible(false);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(true);
				for (final Product p:w.getProdus()){
					int i=Integer.parseInt(textField_4.getText());
					if(p.getCantitate()>i){
					tableModel2.addRow(new Object[]{p.getNumeProdus(),p.getCantitate(),p.getPret()});
					}
				}
			}
		});
		btnOverStock.setBounds(223, 77, 137, 23);
		Administrator.add(btnOverStock);
		
		JButton btnInapoi_3 = new JButton("Inapoi");
		btnInapoi_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.setVisible(false);
				Administrator.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnInapoi_3.setBounds(10, 227, 89, 23);
		OverStock.add(btnInapoi_3);
		
		JButton btnStergereProdus = new JButton("Stergere Produs");
		btnStergereProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				  
				   String n=textField_5.getText();
				 
				   for (final Product p:w.getProdus()){
					  
					   String nume=p.getNumeProdus();
					   if (nume.equals(n)){
						   w.stergeProdus(p);
					   }
					  
				   }
			}
		});
		btnStergereProdus.setBounds(25, 80, 137, 23);
		Administrator.add(btnStergereProdus);
		
		JButton btnInapoi_5 = new JButton("Inapoi");
		btnInapoi_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.setVisible(true);
				Administrator.setVisible(false);
				Client.setVisible(false);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnInapoi_5.setBounds(10, 238, 89, 23);
		Administrator.add(btnInapoi_5);
		
		JButton btnModificapret = new JButton("ModificaPret");
		btnModificapret.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String n=textField_10.getText();
				Integer i=Integer.parseInt(textField_11.getText());
				for(final Product p:w.getProdus()){
					String nume=p.getNumeProdus();
					if(nume.equals(n)){
						p.setPret(i);
					}
				}
			}
		});
		btnModificapret.setBounds(25, 142, 137, 23);
		Administrator.add(btnModificapret);
		
		JButton btnComandaacum = new JButton("ComandaAcum");
		btnComandaacum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String n=textField_6.getText();
				Integer i=Integer.parseInt(textField_7.getText());
				
				for (final Product p:w.getProdus()){
					String nume=p.getNumeProdus();
					Integer j=p.getCantitate()-i;
					Integer v=i*p.getPret();
					
					if(nume.equals(n) && i<=p.getCantitate()){
						p.setCantitate(j);
						tableModel3.addRow(new Object[]{textField_8.getText(),textField_9.getText(),p.getNumeProdus(),i,v});
						
						o.adaugaComanda(new Order(Integer.parseInt(textField_8.getText()),textField_9.getText(),p.getNumeProdus(),i,v));
					    System.out.println("Am adaugat");
					}
					
				}
				o.writeFile();
				 
			}
		});
		btnComandaacum.setBounds(159, 83, 103, 23);
		Client.add(btnComandaacum);
		
	
		
		JButton btnAdministrator = new JButton("Administrator");
		btnAdministrator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				w.readFile();
				Main.setVisible(false);
				Administrator.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnAdministrator.setBounds(161, 76, 128, 23);
		Main.add(btnAdministrator);
		
		JButton btnClient = new JButton("Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				o.readFile();
                w.readFile();
                tableModel3.setRowCount(0);
                for(final Order c:o.getLista_comenzi()){
                	o.writeFile();
                	tableModel3.addRow(new Object[]{c.getId(),c.getNumeClient(),c.getNumeProdus(),c.getCantitate(),c.getPret()});
                }
				Main.setVisible(false);
				Administrator.setVisible(false);
				Client.setVisible(true);
				AdaugareProdus.setVisible(false);
				AfisareProduse.setVisible(false);
				UnderStock.setVisible(false);
				OverStock.setVisible(false);
			}
		});
		btnClient.setBounds(161, 142, 128, 23);
		Main.add(btnClient);
		
		
			
		
	}
}
