import java.io.Serializable;

public class Product implements Serializable,Comparable<Product> {
   
	private String numeProdus;
	private int cantitate;
	private int pret;
	
	public Product(){
		
	}
	
	public Product(String nume, Integer cant, Integer pretul){
		numeProdus=nume;
		cantitate=cant;
		pret=pretul;
		
	}
	
	public String getNumeProdus() {
		return numeProdus;
	}
	
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	
	public int getCantitate() {
		return cantitate;
	}
	
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	
	public int getPret() {
		return pret;
	}
	
	public void setPret(int pret) {
		this.pret = pret;
	}
	
	@Override
	public int compareTo(Product produs) {
		return numeProdus.compareTo(produs.numeProdus);
		
	}
	
	
}
