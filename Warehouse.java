import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

import javax.swing.JTable;

public class Warehouse {
    
	
	TreeSet<Product> produse;
	private String fileName;
	Scanner x=new Scanner(System.in);
	
	public Warehouse(){
	 this.fileName="data.bin";
	 this.produse=new TreeSet<Product>();
		Product p=new Product("Salam",10,12);
		Product p1=new Product("Pufuleti",15,2);
		Product p2=new Product("Unt",13,5);
		Product p3=new Product("Suc",50,6);
		Product p4=new Product("Inghetata",33,3);
		produse.add(p);
		produse.add(p1);
		produse.add(p2);
		produse.add(p3);
		produse.add(p4);
	}
	
	public TreeSet<Product> getProdus() {
		return produse;
	}
  
	public void setProdus(TreeSet<Product> produse) {
		this.produse = produse;
	}
	
	public void adaugaProdus(Product p){
		produse.add(p);
	}
	public void stergeProdus(Product p){
		produse.remove(p);
	}
	
	
	public void writeFile(){
		
	try{
	ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(fileName));
	Iterator<Product> i=produse.iterator();
	
	while(i.hasNext()){
		os.writeObject(i.next());
		System.out.println("Am scris in " + fileName);
	}
	os.close();
	} catch (IOException i) {
		   i.printStackTrace();
		   
		  }
	}
	
	public void readFile() 
	 {
		 System.out.println("Citim din " + fileName);
		  try {
		   ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
		   Product p = null;
		   try {
		    try {
		     while ((p =(Product)in.readObject()) != null) 
		     {		    	 
		       this.adaugaProdus(p);
		     }
		     in.close();
		    } catch (EOFException e) {

		    }
		   } catch (ClassNotFoundException e1) {
		    System.out.println("acuma dam fail");
		    e1.printStackTrace();
		   }
		  } catch (IOException i) {
		   System.out.println("Product class not found");
		   i.printStackTrace();
		   return;
		  }

		 }
	
}