import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.TreeSet;

public class OPDept {

	private TreeSet<Order> lista_comenzi;// sir de comenzi
	private String fileName;// fisieru in care salvam datele
	
	public OPDept ()
	{
		this.lista_comenzi=new TreeSet<Order>();
		this.fileName="comenzi_new20.bin";
	}

	public TreeSet<Order> getLista_comenzi() {
		return lista_comenzi;
	}

	public void setLista_comenzi(TreeSet<Order> lista_comenzi) {
		this.lista_comenzi = lista_comenzi;
	}
	
	public void adaugaComanda(Order o)
	{
		this.lista_comenzi.add(o);
	}
	public void stergeComanda(Order o)
	{
		this.lista_comenzi.remove(o);
	}
	
	 public void writeFile() 
	 {
	   System.out.println("trebuie scris in " + fileName);
	  try {
	   ObjectOutputStream out = new ObjectOutputStream(
	     new FileOutputStream(fileName));

	   Iterator<Order> i = lista_comenzi.iterator();
	   while (i.hasNext()) {
	    out.writeObject(i.next());
	     System.out.println("Am scris in " + fileName);
	   }
	   out.close();
	  } catch (IOException i) {
	   i.printStackTrace();
	  }
	 }
	 
	 public void readFile() 
	 {
		   System.out.println("Citim din " + fileName);
		  try {
			  ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
			  Order o = null;
			  try {
				  try {
					  	while ((o = (Order) in.readObject()) != null) 
					  	{ 
					  		this.adaugaComanda(o);
					  	}
					  	in.close();
				  		} catch (EOFException e) {

				  		}
		   } catch (ClassNotFoundException e1) {
		    System.out.println("acuma dam fail");
		    e1.printStackTrace();
		   }
		  } catch (IOException i) {
		   System.out.println("Product class not found");
		   i.printStackTrace();
		   return;
		  }
		 }
	 
}
