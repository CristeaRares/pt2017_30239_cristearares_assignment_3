import java.io.Serializable;

public class Order implements Serializable,Comparable<Order> {
   

	private int id;
	private String numeClient;
	private String numeProdus;
	private int cantitate;
	private int pret;
	
	public Order(int id,String numeClient,String numeProdus, int cantitate,int pret){
		this.id=id;
		this.numeClient=numeClient;
		this.numeProdus=numeProdus;
		this.cantitate=cantitate;
		this.pret=pret;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNumeClient() {
		return numeClient;
	}
	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}
	public String getNumeProdus() {
		return numeProdus;
	}
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	@Override
	public int compareTo(Order comanda) {
		return numeProdus.compareTo(comanda.numeProdus);
		
	}

}
